from __future__ import division
import numpy as np
from math import log10, floor, ceil

@np.vectorize
def leading_digit(x):
	return int(floor(log10(abs(x))))

@np.vectorize
def absolute_error(x, x_):
	return abs(x - x_)

@np.vectorize
def relative_error(x, x_):
	return absolute_error(x, x_) / x

@np.vectorize
def _rightmost_digit(x, x_):
	if absolute_error(x, x_) != 0.:
		return -log10(absolute_error(x, x_)/5)
	return float('inf')

@np.vectorize
def rightmost_digit(x, x_):
	if absolute_error(x, x_) != 0.:
		return floor(_rightmost_digit(x, x_))
	return float('inf')

@np.vectorize
def _max_significant_digit(x, x_):
	return leading_digit(x) + _rightmost_digit(x, x_)

@np.vectorize
def max_significant_digit(x, x_):
	return floor(_max_significant_digit(x, x_))

@np.vectorize
def round_sigfigs(x, sig_figs):
	if x != 0.:
		return round(x, -leading_digit - (sig_figs - 1))
	return 0