# USAGE

## pair AOD/D3PD/LHE Events by PDFInfo(Q, x1, x2).

```sh
usage: EventMatching [-h] [-f FORMAT PATH] [-v VERBOSITY] [-s SAMPLE_SIZE]
                     [-m METHODS [METHODS ...]] [-l [LOG]]

pair AOD/D3PD/LHE Events by PDFInfo(Q, x1, x2).

optional arguments:
  -h, --help            show this help message and exit
  -f FORMAT PATH, --format FORMAT PATH
                        <FORMAT> is the file format. Can be either {AOD, LHE,
                        StandardLHE, NTUPTruth, NTUPLE} in current version.
                        <PATH> is the file path.
  -v VERBOSITY, --verbosity VERBOSITY
                        Increase/Decrease output verbosity to <VERBOSITY>
                        (default: DEBUG)
  -s SAMPLE_SIZE, --sample_size SAMPLE_SIZE
                        Only take the first <SAMPLE_SIZE> xAOD & LHE Events as
                        samples (default: None)
  -m METHODS [METHODS ...], --methods METHODS [METHODS ...]
                        The matching methods. Can be either or both {pdfInfo,
                        ttbarPz} (default: ['pdfInfo'])
  -l [LOG], --log [LOG]
                        Output a Logfile (default: False)
```

# EXAMPLE

## Generate Sample

1. On local machine:

	```bash
	#LHE -> EVNT
	Generate_tf.py --jobConfig=convertaMcAtNloLHEPythia8_2_EVNT.py --outputEVNTFile=H2tt.EVNT.root --inputGeneratorFile=H2tt --runNumber 31416 --ecmEnergy 13000
	# EVNT -> DAOD TRUTH1
	Reco_tf.py --inputEVNTFile H2tt.EVNT.root --outputDAODFile H2tt.DAOD_TRUTH1.root --reductionConf TRUTH1
	```
	
2. On grid:

	```bash
	pathena --trf="Generate_tf.py --jobConfig=convertaMcAtNloLHEPythia8_2_EVNT.py --outputEVNTFile=%OUT.EVNT --inputGeneratorFile=H2tt --runNumber 31416 --ecmEnergy 13000" --outDS=user.yuchen.H2tt10k.EVNT
	```

## Do the Matching

```bash
rcsetup # Set up RootCore first
python EventMatching -f AOD H2tt.DAOD_TRUTH1.root -f LHE H2tt.lhe.events
```
