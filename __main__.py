from TreeReader import TreeReader
import argparse
import Events
import colorlog
import re

parser = argparse.ArgumentParser(description='pair AOD/D3PD/LHE Events by PDFInfo(Q, x1, x2).',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-f", "--format" ,type = str, action = "append", nargs = 2, metavar = ("FORMAT", "PATH"), help = "<FORMAT> is the file format. Can be either {AOD, LHE, StandardLHE, NTUPTruth, NTUPLE} in current version.\n<PATH> is the file path.")
parser.add_argument("-v", "--verbosity", type = str, default = "DEBUG", help = "Increase/Decrease output verbosity to <VERBOSITY>")
parser.add_argument("-s", "--sample_size",type = int, default = None, help = "Only take the first <SAMPLE_SIZE> xAOD & LHE Events as samples")
parser.add_argument("-m", "--methods", type = str, nargs = "+", default = ["pdfInfo"], help = "The matching methods. Can be either or both {pdfInfo, ttbarPz}")
parser.add_argument("-l", "--log", nargs = "?", type = bool, const = True, default = False, help = "Output a Logfile")

args = parser.parse_args()

def main():
    if args.log:
        logfile_name = '%s_%s.match.log' % (args.format[0][1], args.format[1][1])
        colorlog.logging.basicConfig(filename = logfile_name)
    Events.logger.level = getattr(Events.logging, args.verbosity)
    methods = set(args.methods)

    tree1 = TreeReader(args.format[0][1], args.format[0][0], methods, samplesize = args.sample_size)()
    tree2 = TreeReader(args.format[1][1], args.format[1][0], methods, samplesize = args.sample_size)()

    tree1.match(tree2)
    if args.log:
        with open(logfile_name, "r+") as log_file:
            ansi_escape = re.compile(r'\x1b[^m]*m')
            log = ansi_escape.sub("", "".join(log_file.readlines()))
            log_file.write(log)

main()