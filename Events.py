from __future__ import absolute_import
from collections import namedtuple, Counter
import logging
from scipy.spatial import cKDTree as KDTree
import warnings
warnings.simplefilter("always", RuntimeWarning)
import sys

tol = (5e-4, 5e-6, 5e-6, 5e-4, 5e-4)
TREE_SIZE = 1000

msgfmt = '%(asctime)s %(levelname)-7s %(name)-20s %(message)s'
datefmt = '%H:%M:%S'


def getLogger(name = None, level = logging.DEBUG):
    logger = logging.getLogger(name)
    try:
        import coloredlogs
        coloredlogs.install(logger = logger, level = level, fmt = msgfmt, datefmt = datefmt)
    except ImportError:
        logging.basicConfig(format = msgfmt, datefmt = datefmt)
        logger.setLevel(level)
    return logger
logger = getLogger('EventMatching.Events')


ESC_CODE = {
    'black': '\x1b[30m',
    'red': '\x1b[31m',
    'green': '\x1b[32m',
    'yellow': '\x1b[33m',
    'blue': '\x1b[34m',
    'purple': '\x1b[35m',
    'cyan': '\x1b[36m',
    'white': '\x1b[37m',
    'reset': '\x1b[0m'
    }

try:
    try:
        __IPYTHON__
    except NameError:
        from IPython.core.ultratb import AutoFormattedTB
        sys.excepthook = AutoFormattedTB()
except ImportError:
    logger.warn('IPython is not installed. Colored Traceback will not be populated.')

def fmt(ev):
    return [ev.Q/tol[0], ev.x_max/tol[1], ev.x_min/tol[2]]

def match(x, y, fmt = fmt, clean = False):
    if clean:
        x.clean(), y.clean()

    logger.info("{:=^70}".format("Launch"))

    x_tree = KDTree(map(fmt, x), TREE_SIZE)
    y_tree = KDTree(map(fmt, y), TREE_SIZE)
    for i, matches_i in enumerate(y_tree.query_ball_tree(x_tree, 1., p = float("inf"))):
        for j in matches_i:
            y[i].matches.append(x[j])
            x[j].matches.append(y[i])

    logger.info("{:=^70}".format("End"))

    logger.info("{:=^70}".format("Summary"))
    if logger.level <= logging.INFO:
        logger.info("{:#^34}  {:#^34}".format("  " + x.description + "  ", "  " + y.description + "  "))
        counter1, counter2 = Counter(len(ev.matches) for ev in x), Counter(len(ev.matches) for ev in y)
        for m in set(counter1.keys() + counter2.keys()):
            logger.info(
                "#" + (("{green}" if m == 1 else "{red}") + "{:<2}MATCH: {:>8}/{:<8}".format(m, counter1[m], len(x)).center(32) + "{reset}").format(**ESC_CODE) + "#" +\
                "  " +\
                "#" + (("{green}" if m == 1 else "{red}") + "{:<2}MATCH: {:>8}/{:<8}".format(m, counter2[m], len(y)).center(32) + "{reset}").format(**ESC_CODE) + "#")
        logger.info("{:#^70}".format("  "))
        if counter1[1] != sum(counter1.values()):
            warnings.warn("Not all events find exactly 1 match!", RuntimeWarning)
    if logger.level <= logging.DEBUG:
        fields = [f for f in x[0]._fields if getattr(x[0], f) != None]
        logger.debug("{:=^70}".format("Double checking with " + ", ".join(fields)))
        ALL_SUCCESS = True
        for x_i in x:
            for y_j in x_i.matches:
                if any((x_i[a] - y_j[a] >= tol[a]) if (f not in ("x1", "x2")) else ([x_i.x_max - y_j.x_max, x_i.x_min - y_j.x_min][a - 1] >= tol[a]) for a, f in enumerate(fields)):
                    ALL_SUCCESS = False
                    logger.debug("{red}Check Failed!{reset}".format(**ESC_CODE))
                    logger.debug(x.description + " " + str(x_i))
                    logger.debug(x.description + " " + str(y_j))
        if ALL_SUCCESS:
            logger.debug(("{green}"+"{:^70}".format("All Success!")+"{reset}").format(**ESC_CODE))
        for i in (i for i in range(m + 1) if i != 1):
            logger.debug("{:=^70}".format("%i Match list" % i))
            for ev1, match_ev in ((x_i, x_i.matches) for x_i in x if len(x_i.matches) == i):
                logger.debug("%s: %s" ,ev1, ", ".join(map(str, fmt(ev1))))
                for ev2 in match_ev:
                    logger.debug("%s: %s" ,ev2, ", ".join(map(str, fmt(ev2))))
                logger.debug("")

class Event(namedtuple("Event", ("Q", "x1", "x2", "pz_t", "pz_T"))):
    def __new__(cls, Q, x1, x2, pz_t = None, pz_T = None, event_id = None, fmt = "%.8e", description = ""):
        inst = super(Event, cls).__new__(cls, Q, x1, x2, pz_t, pz_T)
        inst.fmt = fmt
        inst.event_id = event_id
        inst.eventNumber = -999 # -999 means no eventNumber is given to this event
        inst.description = ""
        inst.matches = Events()
        return inst
    def __repr__(self):
        body = ", ".join(("%s=" + self.fmt) % (k ,v) for k, v in zip(self._fields, self) if v != None)
        return "%sEvent<%05i>(%s)" % ((self.description + " ").rjust(5) if self.description else "", self.event_id, body)

    def clean(self):
        self.matches = Events()
    @property
    def x_max(self):
        return max(self.x1, self.x2)
    @property
    def x_min(self):
        return min(self.x1, self.x2)

class Events(list):
    def __new__(cls, *arg):
        inst = super(Events, cls).__new__(cls)
        if len(inst) and all(item == inst[0] for item in inst):
            inst.description = inst[0]
        else:
            inst.description = ""
        return inst

    def match(x, y, fmt = fmt, clean = True):
        match(x, y, fmt = fmt, clean = clean)

    def clean(self):
        map(Event.clean, self)

    def get_logger(self):
        return logger