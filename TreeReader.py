from Events import Event, Events, getLogger
import xml.etree.cElementTree as ET
import csv
import ROOT
logger = getLogger('TreeReader')

class TreeReader(object):
    def __init__(self, filename, filetype, callables = ["pdfInfo"], description = None, samplesize = None):
        self.filename = filename
        self.filetype = filetype.upper()
        self.description = description if description is not None else self.filetype
        self.samplesize = samplesize
        self.callables = callables

    def eventloop(self, callables = None, description = None, samplesize = None):
        if callables is None:
            callables = self.callables
        if description is None:
            description = self.description
        if samplesize is None:
            samplesize = self.samplesize
        try:
            return globals()["_".join(["eventloop", self.filetype])](self, callables, description, samplesize)
        except Exception as e:
            if self.filetype == "LHE":
                logger.info("Not in LHE format. Try to read it as StandardLHE.")
                self.filetype = "STANDARDLHE"
                return self.eventloop(callables, description, samplesize)
            else:
                raise e
        # return globals()["_".join(["eventloop", self.filetype])](self, callables, description, samplesize)


    def __call__(self, *arg, **kwarg):
        return self.eventloop(*arg, **kwarg)

    @staticmethod
    def get_logger():
        return logger

# AOD Helper Functions
def eventloop_AOD(tree_reader, callables, description, samplesize):
    callables = [globals()["_".join(["get", "AOD", c])] if type(c) is str else c for c in callables]
    of = ROOT.TFile(tree_reader.filename)
    CollectionTree = ROOT.xAOD.MakeTransientTree(of)
    events = Events()
    events.description = description
    for i in range(CollectionTree.GetEntries() if not samplesize else samplesize):
        CollectionTree.GetEntry(i)
        event_buffer = dict(event_id = i + 1, description = description)
        for c in callables:
            event_buffer.update(c(CollectionTree))
        ev = Event(**event_buffer)
        ev.eventNumber = CollectionTree.EventInfo.eventNumber()
        events.append(ev)
    ROOT.xAOD.ClearTransientTrees()
    of.Close()
    return events

def get_AOD_pdfInfo(tree):
    pdfInfo = tree.TruthEvents.begin().pdfInfo()
    return {"Q": pdfInfo.Q, "x1": pdfInfo.x1, "x2": pdfInfo.x2}

def get_AOD_ttbarPz(tree):
    T = 6
    TBAR = -6

    for vtx in tree.TruthVertices:
        top_pair = {T: [], TBAR: []}
        for p in vtx.outgoingParticleLinks():
            if p.isValid() and p.isTop():
                top_pair[p.pdgId()].append(p.pz())
        if (len(top_pair[T]) == 1) and (len(top_pair[TBAR]) == 1):
            return {"pz_t": top_pair[T][0]/1000, "pz_T": top_pair[TBAR][0]/1000}



# NTuple Helper Functions
def eventloop_NTUPLE(tree_reader, callables, description, samplesize):
    callables = [globals()["_".join(["get", "NTUPLE", c])] if type(c) is str else c for c in callables]
    of = ROOT.TFile(tree_reader.filename)
    CollectionTree = of.top
    events = Events()
    events.description = description    
    for i in range(CollectionTree.GetEntries() if not samplesize else samplesize):
        CollectionTree.GetEntry(i)
        event_buffer = dict(event_id = i + 1, description = description)
        for c in callables:
            event_buffer.update(c(CollectionTree))
        ev = Event(**event_buffer)
        ev.eventNumber = CollectionTree.eventNumber
        events.append(ev)
    of.Close()
    return events       

def get_NTUPLE_pdfInfo(tree):
    return {"Q": tree.pdf_scale, "x1": tree.pdf_x1, "x2": tree.pdf_x2}

def get_NTUPLE_ttbarPz(tree):
    v_t = ROOT.TLorentzVector()
    v_T = ROOT.TLorentzVector()
    v_t.SetPtEtaPhiM(tree.top_pt, tree.top_eta, tree.top_phi, tree.top_m)
    v_T.SetPtEtaPhiM(tree.tbar_pt, tree.tbar_eta, tree.tbar_phi, tree.tbar_m)
    return {"pz_t": v_t.Pz()/1000, "pz_T": v_T.Pz()/1000}

# Standard LHE Helper Functions
def eventloop_STANDARDLHE(tree_reader, callables, description, samplesize):
    callables = [globals()["_".join(["get", "STANDARDLHE", c])] if type(c) is str else c for c in callables]
    events = Events()
    events.description = description
    root = ET.parse(tree_reader.filename).getroot()
    P1, P2 = map(float, next(root.iter("init")).text.splitlines()[1].split()[2:4])
    for i, event in enumerate(root.iter("event")):
        if samplesize and (i >= samplesize):
            break
        event_buffer = dict(event_id = i + 1, description = description)
        for c in callables:
            event_buffer.update(c(event, P1, P2))
        events.append(Event(**event_buffer))
    return events

def get_STANDARDLHE_pdfInfo(tree, *P):
    head_fieldnames = ["num_particles","status","factorization_scale","Q","alpha1","alpha2"]
    head = next(csv.DictReader(tree.text.splitlines()[1:2], fieldnames = head_fieldnames, delimiter = " ", skipinitialspace = True))
    Q = float(head["Q"])
    fieldnames = ["pdg_id","status","parent_1","parent_2","color_flow_1","color_flow_2","px","py","pz","E","m","spin_1","spin_2"]
    x1, x2 = (float(p["E"]) for p in csv.DictReader(tree.text.splitlines()[2:], fieldnames = fieldnames, delimiter = " ", skipinitialspace = True) if p["status"] == "-1")
    return {"Q": Q, "x1": x1/P[0], "x2": x2/P[1]}

def get_STANDARDLHE_ttbarPz(tree, *P):
    return get_LHE_ttbarPz(tree)

# TruthNTuple Helper Functions
def eventloop_NTUPTRUTH(tree_reader, callables, description, samplesize):
    callables = [globals()["_".join(["get", "NTUPTRUTH", c])] if type(c) is str else c for c in callables]
    of = ROOT.TFile(tree_reader.filename)
    CollectionTree = of.truth
    events = Events()
    events.description = description    
    for i in range(CollectionTree.GetEntries() if not samplesize else samplesize):
        CollectionTree.GetEntry(i)
        event_buffer = dict(event_id = i + 1, description = description)
        for c in callables:
            event_buffer.update(c(CollectionTree))
        events.append(Event(**event_buffer))
    of.Close()
    return events          

def get_NTUPTRUTH_pdfInfo(tree):
    return {"Q": tree.mcevt_pdf_scale[0], "x1": tree.mcevt_pdf_x1[0], "x2": tree.mcevt_pdf_x2[0]}

# CommonNTuple Helper Functions
def eventloop_NTUPCOMMON(tree_reader, callables, description, samplesize):
    callables = [globals()["_".join(["get", "NTUPCOMMON", c])] if type(c) is str else c for c in callables]
    of = ROOT.TFile(tree_reader.filename)
    CollectionTree = of.physics
    events = Events()
    events.description = description
    for i in range(CollectionTree.GetEntries() if not samplesize else samplesize):
        CollectionTree.GetEntry(i)
        event_buffer = dict(event_id = i + 1, description = description)
        for c in callables:
            event_buffer.update(c(CollectionTree))
        events.append(Event(**event_buffer))
    of.Close()
    return events

def get_NTUPCOMMON_pdfInfo(tree):
    return get_NTUPTRUTH_pdfInfo(tree)

try:
    import madgraph.various.lhe_parser as lhe_parser
    def eventloop_LHE(tree_reader, callables, description, samplesize):
        callables = [globals()["_".join(["get", "LHE", c])] if type(c) is str else c for c in callables]
        events = Events()
        events.description = description
        event_file = lhe_parser.EventFile(tree_reader.filename)
        P1, P2 = map(float, event_file.get_banner()["init"].splitlines()[0].split()[2:4])
        for i, event in enumerate(event_file):
            if samplesize and (i >= samplesize):
                break
            event_buffer = dict(event_id = i + 1, description = description)
            for c in callables:
                event_buffer.update(c(event, P1, P2))
            events.append(Event(**event_buffer))
        return events

    def get_LHE_pdfInfo(tree, *P):
    	nincoming = 0
    	ret = {"Q": tree.scale, "x1": 0., "x2": 0.}
    	for p in tree:
    		if p.status == -1:
    			nincoming += 1
    			ret["x%i"%nincoming] = p.E/P[nincoming-1]
    		if nincoming == 2:
    			break
        return ret

    def get_LHE_ttbarPz(tree):
        raise NotImplementedError
        T = "6"
        TBAR = "-6"
        fieldnames = ["pdg_id","status","parent_1","parent_2","color_flow_1","color_flow_2","px","py","pz","E","m","spin_1","spin_2"]
        top_pair = {T: [], TBAR: []}
        for p in csv.DictReader(tree.text.splitlines()[2:], fieldnames = fieldnames, delimiter = " ", skipinitialspace = True):
            if p["pdg_id"] in top_pair:
                top_pair[p["pdg_id"]].append(float(p["pz"]))
        return {"pz_t": top_pair[T][0], "pz_T": top_pair[TBAR][0]}


except:
    raise ImportWarning("Madgraph module is not found. Original lhe_parser is used instead")
    def eventloop_LHE(tree_reader, callables, description, samplesize):
        callables = [globals()["_".join(["get", "LHE", c])] if type(c) is str else c for c in callables]
        events = Events()
        events.description = description
        root = ET.parse(tree_reader.filename).getroot()
        for i, event in enumerate(root.iter("event")):
            if samplesize and (i >= samplesize):
                break
            event_buffer = dict(event_id = i + 1, description = description)
            for c in callables:
                event_buffer.update(c(event))
            events.append(Event(**event_buffer))
        return events
    #LHE Helper Functions
    def get_LHE_pdfInfo(tree):
        pdf1, pdf2 = (map(float, pdf.text.split()[-2:]) for pdf in tree.iter("pdfrwt"))
        return {"Q": pdf1[-1], "x1": pdf1[0], "x2": pdf2[0]}

    def get_LHE_ttbarPz(tree):
        T = "6"
        TBAR = "-6"
        fieldnames = ["pdg_id","status","parent_1","parent_2","color_flow_1","color_flow_2","px","py","pz","E","m","spin_1","spin_2"]
        top_pair = {T: [], TBAR: []}
        for p in csv.DictReader(tree.text.splitlines()[2:], fieldnames = fieldnames, delimiter = " ", skipinitialspace = True):
            if p["pdg_id"] in top_pair:
                top_pair[p["pdg_id"]].append(float(p["pz"]))
        return {"pz_t": top_pair[T][0], "pz_T": top_pair[TBAR][0]}
